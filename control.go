package main

import "github.com/hajimehoshi/ebiten/v2"

type Control struct {
	up, down ebiten.Key
}

func (c *Control) IsUpPressed() bool {
	return ebiten.IsKeyPressed(c.up)
}

func (c *Control) IsDownPressed() bool {
	return ebiten.IsKeyPressed(c.down)
}

func NewControl(up, down ebiten.Key) *Control {
	return &Control{
		up:   up,
		down: down,
	}
}
