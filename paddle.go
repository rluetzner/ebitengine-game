package main

import (
	"fmt"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

type Paddle struct {
	xPos, yPos int
	speed      float64
	control    *Control
}

func (p *Paddle) move(x, y float64) {
	p.xPos += int(x * p.speed)
	p.yPos += int(y * p.speed)
}

func (p *Paddle) checkCollision(b *Ball) {

	// Ball is on the same height
	if p.yPos < b.yPos+ballRadius && p.yPos+paddleWidth > b.yPos+ballRadius &&
		//  Ball collides from the right
		(b.xPos < p.xPos+paddleWidth && b.xPos+ballRadius > p.xPos+paddleWidth ||
			// Ball collides from the left
			b.xPos+ballRadius > p.xPos && b.xPos < p.xPos) {
		b.reflectX()
		fmt.Println("collision")
	}
}

func (p *Paddle) Update() {
	if p.control.IsUpPressed() {
		if p.yPos > 0 {
			p.move(0., -1.0)
		}
	}
	if p.control.IsDownPressed() {
		if p.yPos+paddleHeight <= scrHeight {
			p.move(0., 1.0)
		}
	}
}

func (p *Paddle) Draw(screen *ebiten.Image) {
	ebitenutil.DrawRect(screen, float64(p.xPos), float64(p.yPos), paddleWidth, paddleHeight, color.RGBA{0xff, 0, 0, 0xff})
}
